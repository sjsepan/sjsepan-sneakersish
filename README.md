# Sneakersish Theme

Sneakersish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-sneakersish_code.png](./images/sjsepan-sneakersish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-sneakersish_codium.png](./images/sjsepan-sneakersish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-sneakersish_codedev.png](./images/sjsepan-sneakersish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-sneakersish_ads.png](./images/sjsepan-sneakersish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-sneakersish_theia.png](./images/sjsepan-sneakersish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-sneakersish_positron.png](./images/sjsepan-sneakersish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/10/2025
