# Sneakersish Color Theme - Change Log

## [0.3.2]

- tweak widget/notification borders and backgrounds

## [0.3.1]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.0]

- brighten contrasting panels
- dim borders
- brighten section header border
- retain v0.2.3 for those that prefer earlier style

## [0.2.3]

- update readme and screenshot

## [0.2.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- fix window border in custom mode

## [0.2.1]

- fix manifest and pub WF

## [0.2.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.1.2 for those who prefer the earlier style

## [0.1.2]

- make badge BG transparent

## [0.1.1]

- swap button.FG/BG

## [0.1.0]

- alter terminal ansi color set for more expression
- retain prior version for those who prefer that style

## [0.0.4]

- dim input placeholder FG

## [0.0.3]

- soften selection highlight border
- brighten selection/line highlight BG
- lighten selection BG
- darken input BG
- brighten input border
- fix terminal cursor FG

## [0.0.2]

- comments need to be brighter
- fix statusbar BG, border for nofolder, etc.
"statusBar.debuggingBackground": "#02061f",
"statusBar.debuggingBorder": "#0d30b0",
"statusBar.noFolderBackground": "#02061f",
"statusBar.noFolderBorder": "#0d30b0",

## [0.0.1]

- Initial release
